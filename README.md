# brutus384

Brute forcing sha384 digest of phrase to start with prefix by changing
number in the text.

## Usage

```bash
$ cargo build --release
$ ./target/release/brutus384 --help
```

## Motivation

Friend had algorithm written in Python for brute-forcing sha-384 hash of text
phrase to start with given number of leading zeroes.

**Example:** Change number `N` in phrase `I love you N times more than you love
me!` so that resulting hash starts with 5 leading zeroes.

He wanted to know how much faster could Rust implementation be and what could be
done to the algorithm to speed up the search.

I wanted to try and play with async/parallel programming in Rust, so I had a go.

## Features

- support use of configurable number of threads
- support changing desired prefix
- support changing phrase
- support debug mode (verbose + sleep)

## Approach

The approach is pretty naive. Spawn N threads, each using different start number
to search through possible solution space without stumbling on each other.

## Notes

I have tried to avoid allocation as much as possible, therefore the code is
using pre-allocated buffer for phrase and sha384 digest and writing formatted
text into buffer using `write()` method.

> **Note:** Phrase buffer size is limited by number of digits in `u128::max()`
> and phrase length. The `sha384` digest size is limited by 96 characters.

Have I been successfull? No idea :-D


## License

This code is licensed under **Do What The F*ck You Want To Public License v2**
