use std::thread;
use sha2::Digest;
use sha2::Sha384;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = match Config::parse()? {
        None    => return Ok(()),
        Some(c) => c,
    };

    println!("thread no. = {}", config.thread_num);
    println!("phrase     = '{}'", config.phrase);
    println!("predicate  = '{}'", config.predicate);

    let mut handles = Vec::new();
    for start in 0..config.thread_num {
        {
            let debug      = config.debug;
            let step       = config.thread_num;
            let predicate  = config.predicate.clone();
            let mut phrase = config.phrase.clone();

            let handle = thread::spawn(move || {
                println!("thread {} started", start);
                let mut counter: u128 = start as u128;
                loop {
                    phrase.generate(counter);

                    let valid = predicate.verify(phrase.digest());
                    if  valid {
                        println!("{} '{}'", phrase.digest(), phrase.buffer());
                        std::process::exit(0);
                    }
                    if debug {
                        println!("{} '{}'", phrase.digest(), phrase.buffer());
                        thread::sleep(std::time::Duration::from_millis(500));
                    }


                    counter += step as u128;
                }
                // println!("thread {} finished", start);
                // counter
            });

            handles.push(handle);
        }
    }

    for handle in handles {
        handle.join().unwrap();
    }

    Ok(())
}

// -------------------------------------------------------------------------------------------------
// Phrase

#[derive(Debug,Clone)]
pub struct Phrase {
    parts:  Vec<String>,
    buffer: String,
    digest: String,
}

impl Phrase {
    pub fn new(text: String) -> Self {
        // 40 is number of digits of u128::max()
        let mut length = 40;
        
        let parts: Vec<String> = text.splitn(2, "{}")
                .into_iter()
                .inspect(|s| length += s.len())
                .map(ToString::to_string)
                .collect();
        Self {
            parts,
            buffer: String::with_capacity(length),
            digest: String::with_capacity(96),
        }
    }

    pub fn buffer(&self) -> &str {
        &self.buffer
    }

    pub fn digest(&self) -> &str {
        &self.digest
    }

    pub fn generate(&mut self, counter: u128) {
        self.generate_phrase(counter);
        self.generate_digest()
    }

    fn generate_phrase(&mut self, counter: u128) {
        self.buffer.clear();

        if let Some(part) = self.parts.get(0) {
            self.buffer.push_str(part.as_ref())
        }

        std::fmt::write(&mut self.buffer, format_args!("{}", counter)).unwrap();

        if let Some(part) = self.parts.get(1) {
            self.buffer.push_str(part.as_ref())
        }
    }

    fn generate_digest(&mut self) {
        let digest = Sha384::digest(self.buffer.as_bytes());

        self.digest.clear();
        std::fmt::write(&mut self.digest, format_args!("{:x}", digest)).unwrap();
    }
}

impl std::fmt::Display for Phrase {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.parts.join("{}"))
    }
}

// -------------------------------------------------------------------------------------------------
// Predicate

#[derive(Debug,Clone)]
pub struct Predicate {
    prefix: String,
}

impl Predicate {

    pub fn new(prefix: String) -> Self {
        Self { prefix }
    }

    pub fn verify(&self, text: &str) -> bool {
        let mut pred_iter = self.prefix.chars();
        let mut text_iter = text.chars();

        loop {
            match (pred_iter.next(), text_iter.next()) {
                // mismatch between predicate and text
                (Some(c1), Some(c2)) if c1 != c2 => return false,

                // text shorted than predicate (should not happen)
                (Some(_), None) => return false,

                // predicate has been fully matched
                (None, _) => return true,

                // try another round
                _ => {}
            }
        }
    }
}

impl std::fmt::Display for Predicate {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "prefix == '{}'", self.prefix)
    }
}

// -------------------------------------------------------------------------------------------------
// Config

#[derive(Debug)]
pub struct Config {
    debug: bool,
    thread_num: usize,
    phrase: Phrase,
    predicate: Predicate,
}

impl Config {
    pub fn parse() -> Result<Option<Self>, Box<dyn std::error::Error>> {
        let mut opts = getopts::Options::new();

        opts.optopt("p", "prefix", "expected prefix of generated digest (default=a55)", "PREFIX");
        opts.optopt("t", "text",   "text to hash (default='I love Rust {} times')", "TEXT");

        opts.optopt("T", "threads", "number of threads to use (default=1)", "INT");

        opts.optflag("h", "help",  "show binary help info");
        opts.optflag("d", "debug", "show debug messages");
        
        let args: Vec<String> = std::env::args().skip(1).collect();
        let matches = opts.parse(args)?;

        if matches.opt_present("h") {
            println!("{}", opts.usage(""));
            Ok(None)
        }
        else {
            let prefix = matches.opt_str("p")
                .unwrap_or_else(|| String::from("b00b1e5"));
            let text   = matches.opt_str("t")
                .unwrap_or_else(|| String::from("I love Rust {} times"));

            Ok(Some(Self {
                debug: matches.opt_present("d"),
                thread_num: matches.opt_str("T")
                    .map(|s| s.parse::<usize>())
                    .unwrap_or_else(|| Ok(1))?,

                phrase: Phrase::new(text),
                predicate: Predicate::new(prefix),
            }))
        }
    }
}
